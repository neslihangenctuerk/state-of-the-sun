This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## General
After clicking a button the time for sunrise and sunset for the current location are requested from the API. API Details can be found below under the section __Tools__.
The button can be only used once and gets disabled afterwards. 
Reload for reuse.

## Implementation Details
The stateful component __SunInfo__ is written with the React class syntax which holds state and is responsible for implementing functions called from child components. 
__Button__ and __SunInfoCard__ are stateless meaning they are functional components.
These receive their content that they display and their functions they can call via props. These props are set on component usage in the __SunInfo__ file.
__getSunData__ inside the __SunInfo__ component is using __async__ ___await__ and __fetch__ to requestting the data from the API and afterwards setting it to the local state.

Regarding the __Button__: it could be displayed or hidden depending on the state object property __disableBtn__ but to demonstrate usage of Styled Components in the use case of state dependent styling the template literal syntax was used.

Fonts are declared in __fonts.css__ and imported in the __App.js__.

Color hex values are inside __constants.js__ and only needed colors are imported inside the components. 
Declaring these as const values makes reuse easier and less error prone to possible typos. Expecially inside String interpolations the readability increases. Most apparent in the __Button__ file where some more values could be set via variables (i.e. colors, font sizes or in other files the API baseUrl etc).

Side note: To load the data immediately upon start use the lifecycle method __componentDidMount__.


__next possible TODOS__: 
+ Cover case on API request failure
+ Style Cards further


## Installation and Usage

### `npm install`


### `npm start`
This runs the app in development mode.
Visit [http://localhost:3000](http://localhost:3000) in the browser.
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on Font imports before you run the project via this script. The fonts are not crucial to the functionality of the app.


## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Which is licensed under: [LICENSE](https://github.com/facebook/create-react-app/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.


Styling is done via [Styled Components](https://styled-components.com/). Which is licensed under: [LICENSE](https://github.com/styled-components/styled-components/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.


This project utilizes the [sunrise-sunset.org/api](www.sunrise-sunset.org/api) which requires attribution.

2 Fonts are imported via [Google Fonts](https://fonts.google.com/) - licenses included in each one: 
[Libre Baskerville](https://fonts.google.com/specimen/Libre+Baskerville)
[Lato](https://fonts.google.com/specimen/Lato)
or check the __NOTICE.md__ for a direct link.
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on Font imports before you run the project via the script above. The fonts are not crucial to the functionality of the app.

Please also check the __NOTICE.md__ for more info on licenses.