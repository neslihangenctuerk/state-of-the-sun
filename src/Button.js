import React from 'react'
import styled from 'styled-components'
import { primaryColor, mutedColor } from './constants'

const StyledButton = styled.button`
    font-family: 'Lato', sans-serif;
    background-color: ${props => props.disable ? mutedColor : "white"};
    color: ${props => props.disable ? mutedColor : primaryColor};
    border: 1px solid ${primaryColor};
    font-size: 18px;
    transition: all 400ms ease-in-out 20ms;
    padding: 10px 20px 10px 20px;
    

    &:hover {
        background-color: ${props => props.disabled ? mutedColor : primaryColor};
        color: ${props => props.disabled ? "black" : "white"};
        border: ${props => props.disabled ? mutedColor : `1px solid ${primaryColor}`};
        cursor: ${props => props.disabled ? "not-allowed" : "crosshair"}
    }

`

const Button = props => (
    
        <StyledButton 
            disabled={props.disable} 
            onClick={props.onClick}>
                {props.title}
        </StyledButton>
    
)

export default Button