import React from 'react'
import SunInfoCard from './SunInfoCard'
import Button from './Button'
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    flex-direction:column;
    align-items: center;
    width: 100vw;
    overflow: hidden;
    margin-top: 50px;
`

const CardsContainer = styled.div`
    display: flex;
    flex-direction: row;
    height: 350px;
    margin-top: 50px;
    width: 90vw;
    justify-content: space-between;

` 

class SunInfo extends React.Component {

    state = {
        sunrise: null,
        sunset: null,
        disableBtn: false
    }

    getSunData = () => {
        navigator.geolocation.getCurrentPosition(async position => {
            const lat = position.coords.latitude
            const long = position.coords.longitude
            const response = await fetch(`https://api.sunrise-sunset.org/json?lat=${lat}&lng=${long}`)

            const respJSON = await response.json()
            const sunrise = respJSON.results.sunrise
            const sunset = respJSON.results.sunset

            this.setState((state) => {
                return {
                    sunrise: sunrise,
                    sunset: sunset,
                    disableBtn: true
                }
            })


        })

        /* To avoid requesting on every style change while development:
           use below code as the function body intead 
        */

        // console msg is to verify that the btn is disabled after one click - view README.md for info
        /*console.log("Function __getSunData__ was called")
        this.setState((state) => {
            return {
                sunrise: '04:40 AM',
                sunset: '06:04 PM',
                disableBtn: true
            }
        })*/

    }

    render() {
        return (
            <Container>

                <Button 
                    disable={this.state.disableBtn}
                    title="Get Sunset &amp; Sunrise Info"
                    onClick={this.getSunData} />

               
                {
                    this.state.sunrise &&
                        <CardsContainer>
                            <SunInfoCard 
                                title="Sunrise"
                                suninfo={this.state.sunrise} 
                            />
                                
                            <SunInfoCard 
                                title="Sunset"
                                suninfo={this.state.sunset} 
                            />
                        </CardsContainer>
                }
                
                
            </Container>
        )
    }
}

export default SunInfo