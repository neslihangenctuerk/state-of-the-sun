import React from 'react'
import styled from 'styled-components'
import { accentColor, mutedColor } from './constants'

const Container = styled.div`
    padding: 50px;
    border: 1px solid ${accentColor};
    overflow: hidden;
    display: flex;
    flex-direction: column;
    justify-content: center;
`

const Title = styled.div`
    font-family: 'Lato', sans-serif;
    color: ${mutedColor};
    font-weight: bold;
    font-size: 18px;
    align-self: flex-end;
    padding-right: 5px;
` 

const Time = styled.div`
    font-family: 'Libre Baskerville', serif;
    color: ${accentColor};
    font-size: 85px;
` 

const SunInfoCard = props => (

    <Container>
        <Title>{props.title}</Title>
        <Time>{props.suninfo}</Time>
    </Container>
    
)

export default SunInfoCard